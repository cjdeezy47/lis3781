# LIS3781 - Advanced Database Management

## Christopher DeSimone

### Assignment 5 Requirements:

*Four Parts:*

1. Edit and populate A4 tables on SQL Server with at least 25 records in the sales table and at least 5 records in other tables. 
2. Create a Screenshot of the ERD
3. Create a Screenshot of atleast one SQL statement
4. Add SQL statements to the following code.


#### Assignment Screenshots:

|A5 ERD|
|:-----------------------:|
|![A5 ERD](img/A5_ERD.PNG "A5 SQLServer ERD screenshot")|
 
|Question #1 SQL Statement Solution|
|:-----------------------:|
|![Question #1 Solution](img/sql_statement_1.PNG "Question 1 SQL Statement Solution")|