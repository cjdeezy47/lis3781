# LIS3781 - Advanced Database Management

## Christopher DeSimone

### Project 2 Requirements:

*Three Parts:*

1. Installation of MongoDB and MongoDB Database tools for PC
2. Screenshot of at least *one* MongoDB shell command
3. Screenshot of at least *one* required report and JSON code solution.


#### Assignment Screenshots:

|P2 MongoDB Shell Command|
|:-----------------------:|
|![P2 shell command](img/shell_command.PNG "P2 MongoDB Shell Command")|
 
|P2 Required Report & JSON code solution| 
|:-----------------------:|
|![P2 report/solution](img/report_solution.PNG "P2 Mongodb Report and Solution")|