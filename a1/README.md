# LIS3781 - Advanced Database Management

## Christopher DeSimone

### Assignment 1 Requirements:

*Five Parts:*

1. Distributed Version Control with Git and Bitbucket
2. AMPPS Installation
3. Questions
4. Entity Relationship Diagram, and SQL Code (optional)
5. Bitbucket Repo links:
    a) this assignment and
    b) the completed tutorial (bitbucketlocations)
    
#### A1 Database Business Rules:
The human resource (HR) department of the ACME company wants to contract a database modeler/designer to collect the following employee data for tax purposes: job description, length of employment, benefits, number of dependents and their relationships, DOB of both the employee and any respective dependents. In addition, employees’ histories must be tracked. Also, include the following business rules:

* Each employee may have one or more dependents.
* Each employee has only one job
* Each job can be held by many employees
* Many employees may receive many benefits.
* Many benefits may be selected by many employees (though, while they may not select any benefits—any dependents of employees may be on an employee’s plan).
* Job: title (e.g., secretary, service tech., manager, cashier, janitor, IT, etc.) 
* Benefit: name (e.g., medical, dental, long-term disability, 401k, term life insurance, etc.) 
* Plan: type (single, spouse, family), cost, election date (plans must be unique) 
* Employee history: jobs, salaries, and benefit changes, as well as who made the change and why; 
* Zero Filled data: SSN, zip codes (not phone numbers: US area codes not below 201, NJ); 
* *All* tables must include notes attribute.

In Addtion:

* Employee: SSN, DOB, start/end dates, salary;
* Dependent: same information as their associated employee (though, not start/end dates), date added (as dependent), type of relationship: e.g., father, mother, etc.
* Job: title (e.g., secretary, service tech., manager, cashier, janitor, IT, etc.) 
* Benefit: name (e.g., medical, dental, long-term disability, 401k, term life insurance, etc.) 
* Plan: type (single, spouse, family), cost, election date (plans must be unique) 
* Employee history: jobs, salaries, and benefit changes, as well as who made the change and why;
* Zero Filled data: SSN, zip codes (not phone numbers: US area codes not below 201, NJ); 
* *All* tables must include notes attribute.


#### README.md file should include the following items:
* Screenshot of A1 ERD
* Ex.1 SQL Solution
* git commands w/short descriptions

###   Git Commands w/short descriptions:

1. git intit - Creates new Git repository. 
2. git status - Displays the state of the working directory and the staging area.
3. git add - Adds a change in the working directory to the staging area.
4. git commit - Saves changes to the local repository.
5. git push - Uploads local repository to a remote repository.
6. git pull - Fetches and downloads from a remote repository then updates the local repository.
7. git branch - Lists all branches in your repo, tells you what branch you're currently in.

#### Assignment Screenshots:

* Ampps image

![AMPPS Installation Screenshot](img/ampps.PNG "ampps image")


* A1 ERD

![A1 ERD Screenshot](img/A1_erd.PNG "A1 ERD")


* Question 1 image

![Ex 1](img/ex_1.PNG "Question 1 image")


* Question 2 image

![Ex 2](img/ex_2.PNG "Question 2 image")


* Question 3 image

![Ex 3](img/ex_3.PNG "Question 3 image")


* Question 4 image

![Ex 4](img/ex_4.PNG "Question 4 image")


* Question 5 image

![Ex 5](img/ex_5.PNG "Question 5 image")


* Question 6 image

![Ex 6](img/ex_6.PNG "Question 6 image")

* Question 7 image

![Ex 7](img/ex_7.PNG "Question 7 image")

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/cjdeezy47/bitbucketstationlocations/ "Bitbucket Station Locations")

