
# LIS3781 - Advanced Database Management

## Christopher DeSimone

### LIS3781 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install AMPPS
    - Provide screenshots of installations
    - Create a Bitbucket repo
    - Complete Bitbucket tutorial (bitbucketstationlocations)
    - Provide git command descriptions
2. [A2 README.md](a2/README.md "My A2 README.md file")
    - SQL Code + Screenshots
    - Populate SQL tables using Windows CMD on local and remote servers
    - Questions
3. [A3 README.md](a3/README.md "My A3 README.md file")
    - SQL Code Screenshots
    - Populated tables within Oracle Environment Screenshots
    - SQL Report Questions
4. [P1 README.md](p1/README.md "My P1 README.md file")
    - MySQL MWB File of ERD
    - MySQL screenshot of ERD.
    - MySQL screenshot of 'person' table.
5. [A4 README.md](a4/README.md "My A4 README.md file")
    - SQL Server code
    - ERD
    - SQL Statements
6. [A5 README.md](a5/README.md "My A5 README.md file")
    - A4 SQL Server code w/ additional tables
    - ERD screenshot
    - One SQL Statement screenshot
    - SQL Statements
7. [P2 README.md](p2/README.md "My P2 README.md file")
    - Installation of MongoDB
    - Screenshot of MongoDB shell command
    - Screenshot of at least *one* required report and JSON code solution.