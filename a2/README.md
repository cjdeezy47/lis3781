# LIS3781 - Advanced Database Management

## Christopher DeSimone

### Assignment 2 Requirements:

*Four Parts:*

1. Tables and insert statements.
2. Include indexes and foreign key SQL statements (see below).
3. Include your query result sets, including grant statements.
4. The following tables should be created and populated with at least 5 records both locally and to the CCI server.


#### Assignment Screenshots:

* **A2 MySQL CODE - Part 1**

![MySQL code 1](img/a2_sql_code_a.PNG "MySQL code image 1") 

* **A2 MySQL CODE - Part 2**

![MySQL code 2](img/a2_sql_code_b.PNG "MySQL code image 2")

* **A2 MySQL CODE - Part 3**

![MySQL code 3](img/a2_sql_code_c.PNG "MySQL code image 3")

* **A2 Populated Tables**

![A2 populated tables](img/a2_populated_tables.PNG "MySQL populated tables")

|User 1 Granted Privileges| User 2 Granted Privileges| 
|:-----------------------:|:-------------------------|
|![User 1 Grants](img/user1_grants.PNG "User 1 Grants IMG")| ![User 2 Grants](img/user2_grants.PNG "User 2 Grants IMG")|