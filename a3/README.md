# LIS3781 - Advanced Database Management

## Christopher DeSimone

### Assignment 3 Requirements:

*Two Parts:*

1. Log into Oracle Server using RemoteLabs: Notes > Oracle SQL Developer Login.
2. Create and populate Oracle tables from the SQL Code given


#### Assignment Screenshots:

* **A3 Oracle SQL Code - Part 1**

![Oracle SQL code 1](img/A3_oracle_code_1.PNG "Oracle SQL code image 1") 

* **A3 Oracle SQL Code - Part 2**

![Oracle SQL code 2](img/A3_oracle_code_2.PNG "Oracle SQL code image 2")

* **A3 Oracle SQL Code - Part 3**

![Oracle SQL code 3](img/A3_oracle_code_3.PNG "Oracle SQL code image 3")

|A3 Populated tables - Part 1| A3 Populated tables - Part 2| 
|:-----------------------:|:-------------------------|
|![A3 populated tables](img/A3_poptable_1.PNG "Oracle SQL populated tables 1")| ![A3 populated tables](img/A3_poptable_2.PNG "Oracle SQL populated tables 2")|

**A3 Populated tables - Part 3**

![A3 populated tables](img/A3_poptable_3.PNG "Oracle SQL populated tables 1")