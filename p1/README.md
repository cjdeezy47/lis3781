# LIS3781 - Advanced Database Management

## Christopher DeSimone

### Project 1 Requirements:

*Three Parts:*

1. Create ERD and inserts of the given MySQL code.
2. Salt and hash data for SSN in 'person' table.
3. Take a screenshot of both the ERD and the code for 'person' table.


#### Assignment Screenshots:

|P1 ERD|
|:-----------------------:|
|![P1 ERD](img/P1_ERD_screenshot.PNG "P1 MySQL ERD screenshot")|
 
|P1 'person' table| 
|:-----------------------:|
|!['person' Table](img/person_table_screenshot.PNG "P1 MySQL 'person' table")|