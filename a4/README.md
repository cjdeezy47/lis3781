# LIS3781 - Advanced Database Management

## Christopher DeSimone

### Assignment 4 Requirements:

*Three Parts:*

1. Create and populate tables on SQL Server with at least 10 records in the person table and at least 5 records in other tables. 
2. Create a creenshot of the ERD
3. Add SQL statements to the following code.


#### Assignment Screenshots:

|A4 ERD|
|:-----------------------:|
|![A4 ERD](img/A4_ERD.PNG "A4 SQLServer ERD screenshot")|
 
